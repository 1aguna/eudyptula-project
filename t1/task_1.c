#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>

static int init_task_1(void) {
    printk(KERN_DEBUG, "Task 1 Init!\n");
    return 0;
}

static void exit_task_1(void) {
    printk(KERN_DEBUG, "Task 1 Exit!\n");
}

module_init(init_task_1);
module_exit(exit_task_1);

