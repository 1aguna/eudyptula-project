#include <linux/module.h>
#include <linux/fs.h>
#include <linux/poll.h>
#include <linux/errno.h>
#include <linux/semeaphore.h>
#include <linux/debugfs.h>
#include <linux/unistd.h>
#include <linux/jiffies.h> // for jiffies file 

#define ID "I have no ID!"
#define ID_LEN 13	/* ID length including the final NULL */

static DEFINE_SEMAPHORE(sem) // init a binary semaphore

static char pages[PAGE_SIZE];
static 


// need to make read with a lock
static ssize_t ocean_read(struct file *file, char __user *buf,
			size_t count, loff_t *ppos)
{
    return simple_read_from_buffer(buf, count, ppos, ID, ID_LEN);
}

static ssize_t ocean_write(struct file *file, char const __user *buf,
			size_t count, loff_t *ppos)
{
    return simple_write_to_buffer(buf, count, ppos, ID, ID_LEN)
}

static int __init hello_init(void)
{

	int i = misc_register(&ocean_hello);

	return i;
}

// module exit must deregister device
static void __exit hello_exit(void)
{
	misc_deregister(&hello_dev);
}

// file operations for driver
static const struct file_operations ocean_fops = {
	.owner = THIS_MODULE,
	.read = ocean_read,
	.write = ocean_write
};

// device information
// minor number is distinct number for physical or logical device
static struct miscdevice ocean_hello = {
	.minor = MISC_DYNAMIC_MINOR, // assign dynamic minor number, no explicit number
	.name = "Ocean's Edupytula",
	.fops = &ocean_fops
};

module_init(hello_init);
module_exit(hello_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("1aguna");
MODULE_DESCRIPTION("Misc char hello world module");

